#include "ESP8266.h"
#include <ArduinoJson.h>

#define RST 4

//Rede que o ESP vai conectar
//#define SSID        "diogo"
//#define PASSWORD    "12345678"

//Rede que o ESP vai criar
#define SSID_C      "espbarco"
#define PASSWORD_C  "12345678"

SoftwareSerial ESP(2, 3);
ESP8266 wifi(ESP);


//Motores
#define E1 11
#define E2 10

#define ML1 8 //motor left
#define ML2 9

#define MR1 5
#define MR2 6



/*
void motores(int motor, int velocidade)
int motor -> motor que será gerenciado  (1 -> esquerda)
                                        (2 -> direita)


int velocidade -> velocidade do motor (velocidade negativa significa sentido inverso)
*/
void motores(int velocidade1, int velocidade2) {
  velocidade1 = ((255*velocidade1)/100);
  velocidade2 = ((255*velocidade2)/100);

  Serial.print("V1: ");
  Serial.print(velocidade1);
  Serial.print(" - V2: ");
  Serial.println(velocidade2);

  if(velocidade1 < 0) {
    digitalWrite(ML1, LOW);
    digitalWrite(ML2, HIGH);
    velocidade1 = velocidade1 * (-1);
  } else {
    digitalWrite(ML2, LOW);
    digitalWrite(ML1, HIGH);
  }

  if(velocidade2 < 0) {
    digitalWrite(MR1, LOW);
    digitalWrite(MR2, HIGH);
    velocidade2 = velocidade2 * (-1);
  } else {
    digitalWrite(MR2, LOW);
    digitalWrite(MR1, HIGH);
  }


  analogWrite(E1, velocidade1);
  analogWrite(E2, velocidade2);
}


void setup(void)
{
    //Pulso no reset para que o ESP aceite conexões
    pinMode(RST, OUTPUT);
    digitalWrite(RST, LOW);
    delay(300);
    digitalWrite(RST, HIGH);

    pinMode(E1, OUTPUT);
    pinMode(E2, OUTPUT);

    pinMode(ML1, OUTPUT);
    pinMode(ML2, OUTPUT);

    pinMode(MR1, OUTPUT);
    pinMode(MR2, OUTPUT);

    Serial.begin(9600);
    Serial.println("Iniciando Configuração - Barco Remote Control.");

    if(wifi.setSoftAPParam(SSID_C, PASSWORD_C)) {
      Serial.println("SoftAP configuracao OK.");
    } else {
      Serial.println("Erro em configurar SoftAP.");
    }

    if(wifi.setOprToSoftAP()) {
        Serial.println("Station e AccessPoint OK.");
    } else {
        Serial.println("Erro em setar Station e AccessPoint.");
    }

    /*if(wifi.joinAP(SSID, PASSWORD)) {
        Serial.println("Conectado com Sucesso.");
        Serial.println("IPs: ");
        Serial.println(wifi.getLocalIP().c_str());
    } else {
        Serial.println("Falha na conexao AP.");
    }*/

    if(wifi.enableMUX()) {
        Serial.println("Multiplas conexoes OK.");
    } else {
        Serial.println("Erro ao setar multiplas conexoes.");
    }

    if(wifi.startTCPServer(8090)) {
        Serial.println("Servidor iniciado com sucesso (porta 8090).");
    } else {
        Serial.println("Erro ao iniciar servidor.");
    }
    Serial.println("Configuração finalizada!");
}

void loop(void)
{
    uint8_t buffer[128] = {0};
    uint8_t mux_id;

    delay(150);
    uint32_t len = wifi.recv(&mux_id, buffer, sizeof(buffer), 100);
    String recebido = "";
    int velomotorL, velomotorR;
    float x, y;

    if(len > 0 && len <= 24) {
        //Serial.println(len);
        //Serial.print("[MUXID: ");
        //Serial.print(mux_id);
        //Serial.print("] - Recebido: ");
        for(uint32_t i = 0; i < len; i++) {
            //Serial.print((char)buffer[i]);
            recebido += (char)buffer[i];
        }
        //Serial.print("]\r\n");

        StaticJsonBuffer<200> jsonBuffer;
        JsonObject& json_recebido = jsonBuffer.parseObject(recebido);

        x = json_recebido["x"];
        y = json_recebido["y"];
        if(y < 0) { //LEFT
            velomotorL = x;
            velomotorR = 10 - (y * (-1));
        } else if(y > 0) {
          velomotorL = 10 - y;
          velomotorR = x;
        } else if(y == 0) {
          velomotorL = 10;
          velomotorR = 10;
        }

        velomotorL*= x;
        velomotorR*= x;

        motores(velomotorL, velomotorR);

        String texto = "X = " + String(x) + " - Y: " + String(y) + "\n";
        Serial.println(texto);

        recebido = "";
    }
}

/*const uint8_t* enviar = reinterpret_cast<const uint8_t*>(texto.c_str());

if(wifi.send(mux_id, enviar, texto.length())) {
    Serial.print("[MUXID: ");
    Serial.print(mux_id);
    Serial.print("] - Enviado!\n");
} else {
    Serial.print("[MUXID: ");
    Serial.print(mux_id);
    Serial.print("] - Erro ao enviar!\n");
}*/
