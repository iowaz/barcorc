package orangepixel.barcorc;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ConectarActivity extends Activity {

    TextView textResponse;
    EditText ipESP, portaESP;
    Button buttonConnect, enviarJSON;

    String ipESP_tratado;
    int portaESP_tratado;

    private TCPClient clienteTCP;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conectar);

        ipESP = (EditText) findViewById(R.id.ipESP);
        portaESP = (EditText) findViewById(R.id.portaESP);
        buttonConnect = (Button) findViewById(R.id.conectar);
        textResponse = (TextView) findViewById(R.id.resposta);
        enviarJSON = (Button) findViewById(R.id.enviarJSON);


        buttonConnect.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     ipESP_tratado = ipESP.getText().toString();
                     portaESP_tratado = Integer.parseInt(portaESP.getText().toString());
                     new connectTask().execute("");

                     Intent myIntent = new Intent(ConectarActivity.this, MainActivity.class);
                     startActivity(myIntent);
                 }
             }
        );
    }


        public class connectTask extends AsyncTask<String, String, TCPClient> {

            @Override
            protected TCPClient doInBackground(String... message) {

                clienteTCP = new TCPClient(new TCPClient.OnMessageReceived() {
                    @Override
                    public void messageReceived(String message) {
                        Log.e("RECEBIDA:", message);
                        publishProgress(message);
                    }
                });

                clienteTCP.run(ipESP_tratado, portaESP_tratado);
                Log.e("Teste", "conectou");

                return null;
            }
            @Override
            protected void onProgressUpdate(String... values) {
                super.onProgressUpdate(values);

                Log.e("teste", values[0]);
            }
        }
    }
