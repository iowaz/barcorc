package orangepixel.barcorc;

import android.app.*;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.*;
import android.view.*;


import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.widget.Button;
import android.widget.TextView;



@SuppressWarnings("unused")
public class MainActivity extends Activity implements OnSeekBarChangeListener {

	private TCPClient clienteTCP;

    TextView textResponse;
    EditText ipESP, portaESP;
    Button buttonConnect, enviarJSON;

    String ipESP_tratado;
    int portaESP_tratado;

    public static final int MENU_CONNECT = Menu.FIRST;
    public static final int MENU_ABOUT = Menu.FIRST + 1;
    public static final int MENU_QUIT = Menu.FIRST + 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		VerticalSeekBar velocidade = (VerticalSeekBar)findViewById(R.id.seekBar_velocidade);
		velocidade.setMax(20);
		velocidade.setProgress(10);
		velocidade.setOnSeekBarChangeListener(this);
		
		SeekBar motor = (SeekBar)findViewById(R.id.seekBar_motor);
		motor.setMax(20);
		motor.setProgress(10);
		motor.setOnSeekBarChangeListener(this);
	}

	int x_velo, y_velo;
	public void onProgressChanged (SeekBar seekBar, int progress, boolean fromUser) {
		if(seekBar == (SeekBar)findViewById(R.id.seekBar_velocidade)) {
			TextView tv = (TextView)findViewById(R.id.textView_velocidade2);
			x_velo = progress-10;
			tv.setText("Velocidade: " + x_velo + "m/s");
		}
		if(seekBar == (SeekBar)findViewById(R.id.seekBar_motor)) {
			TextView tv = (TextView)findViewById(R.id.textView_motor2);
			y_velo = progress-10;
			tv.setText("Rotação motor: " + y_velo + "%");
		}

        final CountDownTimer start = new CountDownTimer(500, 50) {
            public void onTick(long millisUntilFinished) {
                Log.d("Ticker", millisUntilFinished + " para enviar!");
            }

            public void onFinish() {
                String mensagem = "{\"x\":\""+x_velo+"\",\"y\":\""+y_velo+"\"}";
                Log.e("SendMessage", mensagem);
                clienteTCP.sendMessage(mensagem);
            }
        }.start();
    }
	
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
	}
	
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
	}

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, MENU_CONNECT, 0, "Conectar").setIcon(R.drawable.menu_info_connect);
        menu.add(0, MENU_ABOUT, 0, "Sobre").setIcon(R.drawable.menu_info_icon);
        menu.add(0, MENU_QUIT, 0, "Fechar").setIcon(R.drawable.menu_quit_icon);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_CONNECT:
                showConnectDialog();
                return true;
            case MENU_ABOUT:
                showAboutDialog();
                return true;
            case MENU_QUIT:
                finish();
                return true;
        }
        return false;
    }


    private void showConnectDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.connectbox);
        dialog.show();

        ipESP = (EditText) dialog.findViewById(R.id.ipESP);
        portaESP = (EditText) dialog.findViewById(R.id.portaESP);
        buttonConnect = (Button) dialog.findViewById(R.id.conectar);
        textResponse = (TextView) dialog.findViewById(R.id.resposta);

        buttonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ipESP_tratado = ipESP.getText().toString();
                portaESP_tratado = Integer.parseInt(portaESP.getText().toString());
                new connectTask().execute("");
                Log.e("TCPClient", "conectou");
            }
        });
    }

	private void showAboutDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.aboutbox);
        dialog.show();
    }

	public class connectTask extends AsyncTask<String, String, TCPClient> {

		@Override
		protected TCPClient doInBackground(String... message) {

			clienteTCP = new TCPClient(new TCPClient.OnMessageReceived() {
				@Override
				public void messageReceived(String message) {
					Log.e("RECEBIDA:", message);
					publishProgress(message);
				}
			});

			clienteTCP.run(ipESP_tratado, portaESP_tratado);

			return null;
		}
		@Override
		protected void onProgressUpdate(String... values) {
			super.onProgressUpdate(values);

			Log.e("teste", values[0]);
		}
	}
}
